package it.unibo.scafi.simulation.gui.model.sensor

import it.unibo.scafi.simulation.gui.model.common.world.CommonWorldEvent.EventType

object SensorEvent {

  /**
    * an event produced when a sensor value change
    */
  object SensorChanged extends EventType
}
